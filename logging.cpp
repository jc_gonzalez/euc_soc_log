/******************************************************************************
 * File:    logging.cpp
 *          This file is part of QPF
 *
 * Domain:  qpf.fmk.Logging
 *
 * Last update:  1.0
 *
 * Date:    20190614
 *
 * Author:  J C Gonzalez
 *
 * Copyright (C) 2019 Euclid SOC Team / J C Gonzalez
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Implement Logging class
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   TBD
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog> file
 *
 * About: License Conditions
 *   See <License> file
 *
 ******************************************************************************/

#include "logging.h"

//----------------------------------------------------------------------
// Destructor
//----------------------------------------------------------------------
Logging::~Logging()
{
}

//----------------------------------------------------------------------
// Method: initialize
//----------------------------------------------------------------------
void Logging::initialize(std::string propertiesFileName)
{
    log4cpp::PropertyConfigurator::configure(propertiesFileName);
}

//----------------------------------------------------------------------
// Method: getRootLogger
//----------------------------------------------------------------------
Logger Logging::getRootLogger()
{
    return log4cpp::Category::getRoot();
}
   
//----------------------------------------------------------------------
// Method: getLogger
//----------------------------------------------------------------------
Logger Logging::getLogger(std::string name, std::string basedOn)
{
    if (basedOn.empty()) {
        return log4cpp::Category::getInstance(name);
    } else {
        auto & baseCat = log4cpp::Category::getInstance(basedOn);
        auto & newCateg = log4cpp::Category::getInstance(name);
        newCateg.addAppender(baseCat.getAppender());
        return newCateg;
    }
    
}

    
