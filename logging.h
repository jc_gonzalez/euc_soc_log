/******************************************************************************
 * File:    logging.h
 *          This file is part of QPF
 *
 * Domain:  qpf.fmk.Logging
 *
 * Last update:  1.0
 *
 * Date:    20190614
 *
 * Author:  J C Gonzalez
 *
 * Copyright (C) 2019 Euclid SOC Team / J C Gonzalez
 *_____________________________________________________________________________
 *
 * Topic: General Information
 *
 * Purpose:
 *   Declare Logging class
 *
 * Created by:
 *   J C Gonzalez
 *
 * Status:
 *   Prototype
 *
 * Dependencies:
 *   TBD
 *
 * Files read / modified:
 *   none
 *
 * History:
 *   See <Changelog> file
 *
 * About: License Conditions
 *   See <License> file
 *
 ******************************************************************************/

#ifndef LOGGING_H
#define LOGGING_H

//============================================================
// Group: External Dependencies
//============================================================

//------------------------------------------------------------
// Topic: System headers
//   - iostream
//------------------------------------------------------------
#include <log4cpp/Category.hh>
#include <log4cpp/PropertyConfigurator.hh>

#include <string>

//------------------------------------------------------------
// Topic: External packages
//------------------------------------------------------------

//------------------------------------------------------------
// Topic: Project headers
//------------------------------------------------------------

typedef log4cpp::Category& Logger;

//==========================================================================
// Class: Logging
//==========================================================================
class Logging {

public:
    //----------------------------------------------------------------------
    // Destructor
    //----------------------------------------------------------------------
    virtual ~Logging();

    //----------------------------------------------------------------------
    // Method: initialize
    //----------------------------------------------------------------------
    static void initialize(std::string propertiesFileName);

    //----------------------------------------------------------------------
    // Method: initialize
    //----------------------------------------------------------------------
    static Logger getRootLogger();
   
    //----------------------------------------------------------------------
    // Method: initialize
    //----------------------------------------------------------------------
    static Logger getLogger(std::string name, std::string basedOn = std::string());
    
private:
    //----------------------------------------------------------------------
    // Constructor
    //----------------------------------------------------------------------
    Logging() {}
};

#endif // LOGGING_H
